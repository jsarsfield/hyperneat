//
// Created by joe on 06/06/15.
//

#ifndef NEAT_SPECIES_H
#define NEAT_SPECIES_H

#include "Population.h"

class Species {
public:
    Species(int speciesId, int generationCreated ) : speciesId(speciesId), generationCreated(generationCreated) {};

    std::vector<std::shared_ptr<Organism>> organisms;   // The organisms in the Species
    int speciesId;          // id of the species
    int generationCreated = 0;  // The generation this species was created
    int age = 0;                // Age of the species
    int ageOfLastImprovement = 0;   // What age was the last improvement to maxFitnessEver
    int indOfGen = 0;                // Index of last organism from this generation (used during reproduction to ensure next generation organisms added to the species arent used during reproduction))
    precision avgFitness = 0;       // The average fitness of the Species in this generation
    precision totalSpeciesFitness = 0;  // Total fitness of the Species in this generation
    precision maxFitness = 0;      // Original max fitness of the Species in this generation
    precision maxFitnessEver = 0;  // Original max fitness this Species has achieved during its existence
    void AdjustFitness(Population *pop);   // Change the fitness of all the organisms in the species to depend slightly on the age of the species
    void Reproduce(Population *pop);       // Reproduce the next generation of organisms and perform mutations
    int expectedOffspring;  // Number of offspring this species will produce for the new generation
    int numOfSpeciesChampOffspring; // How many offspring should the species champ produce
    int killIndex;              // Index of start of organisms that cannot reproduce because of low fitness (As organisms are sorted by fitness this index and above cannot reproduce)
    std::shared_ptr<Genome> GenomeMatching(std::shared_ptr<Genome> gen1, std::shared_ptr<Genome> gen2,
                                           precision fitness1, precision fitness2); // Matching two genomes to produce a new genome when mating
};


#endif //NEAT_SPECIES_H
