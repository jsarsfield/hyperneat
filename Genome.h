//
// Created by joe on 01/06/15.
//

#ifndef NEAT_GENOME_H
#define NEAT_GENOME_H

#include <vector>
#include <memory>
#include "CustomDatatypes.h"
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>

using namespace std;

class Genome {
public:
    Genome();
    ~Genome();
    int genomeId;                                 // Unique id of the genome
    void AddLocalConnection(std::shared_ptr<GConnection> gConnection, precision weight, bool bIsEnabled);  // Add a local connection to the genome
    void AddLocalNeuron(std::shared_ptr<GNeuron> gNeuron, bool bCheckIfExists);      // Add a local neuron to the genome
    void AddLocalLayer(std::shared_ptr<GLayer> gLayer, bool bCheckIfExists);         // Add a local layer to the genome
    void AddGene(shared_ptr<LConnection> lConnection);   // Add connections/neurons/layers to the genome during crossover
    std::shared_ptr<LLayer> GetLocalLayer(float depth);       // Get local layer
    std::shared_ptr<Genome> Clone();   // Clone this genome

    std::vector<std::shared_ptr<LConnection>> localConnections;   // Array of local connections in this genome
    std::vector<std::shared_ptr<LNeuron>> localNeurons;           // Array of local neurons in this genome
    std::vector<std::shared_ptr<LLayer>> localLayers;             // Array of local layers in this genome

    void MutateAddConnection(Population *pop);                 // Add a connection to a genome
    void MutateAddNeuron(Population *pop);                     // Add a Neuron to a genome
    void MutateWeights(double power, double rate, Mutator mutator);  // Modify weights of connections in a genome
    void MutateEnableCon();                       // Enable a disabled connection
    void MutateDisableCon();                       // Enable a disabled connection

        private:
    friend class cereal::access;

    template <class Archive>
    void serialize( Archive & ar, const unsigned int version ) {
        ar(localConnections, localNeurons, localLayers);
    }

};


#endif //NEAT_GENOME_H
