set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
set(EXECUTABLE_OUTPUT_PATH "${CMAKE_HOME_DIRECTORY}/UnitTests/bin")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_HOME_DIRECTORY}/UnitTests/bin")
add_executable(NEAT_tests TestsMain.cpp)
target_link_libraries (NEAT_tests NEATLib)


include_directories(../../cereal/include/ ../../Catch/include/ ..)