//
// Created by joe on 31/05/15.
//

#ifndef NEAT_POPULATION_H
#define NEAT_POPULATION_H

#include "CustomDatatypes.h"
#include <cereal/types/map.hpp>
#include <cereal/types/string.hpp>
#include <cereal/archives/json.hpp>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <set>
#include <iterator>
#include <experimental/optional>

class Species;
class Organism;
class Genome;

namespace NEAT {
    extern precision disjointCoeff;         // Coefficient for disjoint connections/genes
    extern precision excessCoeff;           // Coefficient for excess connections/genes
    extern precision avgWeighDiffCoeff;     // Coefficient for average weight differences for matching connections/genes
    extern precision distanceThreshold;     // Dynamic variable to control desired num of species   #TODO make this dynamic
    extern int populationSize;          // Size of population to generate for new generation
    extern precision interspeciesMateRate;      // Chance of organism mating with another organism outside of it's species
    extern precision chanceOfMutation;          // Chance of genome being mutated either structurally or connection weights
    extern precision chanceOfRandomWeight;      // Chance of being assigned a completely random weight as opposed to uniform mutation
    extern precision chanceOfInheritDisabledConn; // Chance that inherited gene is disabled if it is disabled in either parent (during crossover)
    extern precision chanceOfEnableConn;        // Chance of enabling connection
    extern precision chanceOfDisableConn;       // Chance of disabling connection
    extern precision chanceOfAverageConnWeight; // Chance of inherited gene taking the average conn weight of both parent genes as opposed to 1 parent randomly (during crossover)
    extern precision chanceOfAddingNeuron;      // Chance of adding a Neuron to an offspring's genome (during crossover)
    extern precision chanceOfAddingConnection;  // Chance of adding a connection to an offspring's genome (during crossover)
    extern precision chanceOfCrossover;         // Chance of crossover occurring. If crossover doesn't occur then weights may be mutated.
    extern precision survivalThreshold;         // Percent of average fitness for survival in species
    extern precision chanceOfInterspeciesMate;  // Chance of selecting a mate from another species
    extern precision chanceOfUseInferiorDisjoint;   // Chance of using inferior genome disjoints during crossover
    void SetupRand();
    extern std::mt19937 rand;                  // Pseudo random number generator seeded random_device in constructor
    extern std::uniform_real_distribution<precision> dist; // Uniform distribution
    extern std::exponential_distribution<precision> expoDistri; // exponential distribution favouring numbers tending to 0
    extern std::uniform_real_distribution<precision> weightDistr;  // Random weight distribution
    extern std::normal_distribution<precision> gaussDistri;       // Gaussian distribution favouring smaller values near 0 for weight changes
    extern precision  GetRandomExp();   // Get value from 0-1 waited towards zero

    bool ChanceOfEvent(precision event);
    bool SortByFitness(std::shared_ptr<Organism> o1, std::shared_ptr<Organism> o2 );
    bool SortByInnovNum(std::shared_ptr<LConnection> c1, std::shared_ptr<LConnection> c2);
    bool SortLayersByDepth(std::shared_ptr<LLayer> l1, std::shared_ptr<LLayer> l2);
}

class Population {
public:
    Population(std::string genomesFile);
    Population(int numOfInputNeurons, int numOfOutputNeurons, int numOfGenomes);
    int GetGlobalInnovationNumber() const { return globalInnovationNumber; }
    void IncrementGlobalInnovNo() { Population::globalInnovationNumber = Population::globalInnovationNumber+1; }
    void AssignInnovationNumber(std::shared_ptr<GConnection> connection);    // Assign innovation number to a gene
    void InitialSpeciate();                            // Speciate a population of organisms
    void Update();                              // Main loop
    void Epoch();                               // Reproduce the next generation of organisms and perform mutations
    bool CheckGenomeDistance(std::shared_ptr<Organism> org1, std::shared_ptr<Organism> org2);   // Check distance between two organisms for speciation
    void LoadInitialPopulation(std::string genomesFile = NULL, int numOfInputNeurons = 0, int numOfOutputNeurons = 0, int numOfGenomes = 0);               // Load initial genomes for spawning initial population. Should be minimal structure if new evolution otherwise reload the genomes from a previous generation.
    void SaveGenomes(std::string saveFile);  // Save genomes to disk
    void LoadGenomes(std::string loadFile);  // Load genomes from disk
    std::vector<int> DistributeConnections(int numOfInputNeurons, int numOfOutputNeurons); //Divide connections equally, ensuring each neuron has a connection
    std::vector<std::shared_ptr<Species>> species;      // Array of species in the population.
    std::vector<std::shared_ptr<Organism>> organisms;   // The organisms in the population
    std::vector<std::shared_ptr<Genome>> genomes;       // UNUSED possibly not necessary as we have list of organisms, the genomes can be accessed through the organisms vector. Genomes in the population
    std::vector<std::shared_ptr<GConnection>> globalConnections;   // Array of connections that have been created since the beginning (Genomes that use specific connections will store the pointers in a local array)
    std::vector<std::shared_ptr<GNeuron>> globalNeurons;           // Array of neurons that have been created since the beginning (Genomes that use specific neurons will store the pointers in a local array)
    std::vector<std::shared_ptr<GLayer>> globalLayers;             // Array of layers that have been created since the beginning (Genomes that use specific layers will store the pointers in a local array)
    std::vector<std::shared_ptr<InnovationAddNeuron>> innovationsAddNeuron; // Array of innovations where a new neuron is added
    std::vector<std::shared_ptr<InnovationAddCon>> innovationsAddCon;   // Array of innovations where a new connection is added
    tuple<bool, shared_ptr<GConnection>>  AddGlobalConnection(std::shared_ptr<GNeuron> in, std::shared_ptr<GNeuron> out);  // Add a global connection to the genome
    std::shared_ptr<GNeuron> AddGlobalNeuron(float depth);          // Add a global neuron to the genome
    std::shared_ptr<GLayer> AddGlobalLayer(float depth);            // Add a global layer to the genome
    shared_ptr<GConnection> AddInnovationConnection(std::shared_ptr<GNeuron> in, std::shared_ptr<GNeuron> out);   // Add connection innovation
    tuple<shared_ptr<GNeuron>,shared_ptr<GConnection>,shared_ptr<GConnection>> AddInnovationNeuron(std::shared_ptr<GConnection> con, shared_ptr<LLayer> layer, float depth);                   // Add neuron innovation, achieved by splitting existing con
    std::experimental::optional<std::shared_ptr<GConnection>> DoesConnectionExist(std::shared_ptr<GNeuron> in, std::shared_ptr<GNeuron> out);   // Does this connection exist, if so return a reference
    std::experimental::optional<std::shared_ptr<GLayer>> DoesLayerExist(float depth);            // Does a layer with this depth exist, if so return a reference
    int generation = 0;     // Generation of the population
    int numberOfSpecies = 0;    // Number of species that have ever existed in the population, including dead species (Used to create a species id)
    void AddSpecies(std::shared_ptr<Organism> organism);  // Add speicies to population
private:
    int globalInnovationNumber = 0;     // Historical marking used for determining suitability for crossover

};

#endif //NEAT_POPULATION_H
