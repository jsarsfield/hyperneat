//
// Created by Joe on 23/12/2015.
// Basic implementation of a convolutional neural network. There is no training stage (life time learning) as the
// topology & weights are selected using HyperNEAT (Genetic algorithm)
//

#ifndef NEAT_CONVNN_H
#define NEAT_CONVNN_H

#include "Organism.h"
#include "Genome.h"
#include "Species.h"

class ConvNN {
public:
    std::shared_ptr<Organism> org;      // organism/phenotype to evaluate
    vector<vector<precision>> activations;  // stores activations of the neural network including output for evaluating fitness
    ConvNN(std::shared_ptr<Organism> org): org(org){GetOutput();};

    void GetOutput();   // Calculate the activations of the neural network
    void CalculateFitness();    // Calculate the fitness of the neural network using the output
};


#endif //NEAT_CONVNN_H
