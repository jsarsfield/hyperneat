//
// Created by joe on 06/06/15.
//

#include "Species.h"
#include "Genome.h"
#include "Organism.h"

using namespace std;

void Species::Reproduce(Population *pop) {
    bool bClonedChamp = false;  // Have we made a clone of the species champ
    // Produce number of expected organisms for this species
    for (int i=0;i<expectedOffspring;i++) {
        shared_ptr<Genome> baby = make_shared<Genome>();    // The baby organism to produce
        bool bMutateWeights = false;
        // Produce clone of species champ, no mutations are performed
        if (!bClonedChamp){
            baby = organisms[0]->genome->Clone();
            bClonedChamp = true;
        }
        else {
            // Choose a mother weighted towards lower values as organisms are sorted based on fitness
            int motherIndex = ceil(NEAT::GetRandomExp()*indOfGen)-1;    // Index of mother organism

            // Should we clone a random organism from the species?
            if (NEAT::ChanceOfEvent(NEAT::chanceOfMutation)) {
                baby = organisms[motherIndex]->genome->Clone();
                bMutateWeights = true;
            }
            // Otherwise mate two random organisms from the species
            else {
                // Mate with an organism outside of this species (There is no check to ensure the father isn't within the mother's species)
                if (NEAT::ChanceOfEvent(NEAT::chanceOfInterspeciesMate)) {
                    int speciesIndex = ceil(NEAT::GetRandomExp()*pop->species.size())-1;
                    int fatherIndex = ceil(NEAT::GetRandomExp()*pop->species[speciesIndex]->indOfGen)-1;
                    baby = GenomeMatching(organisms[motherIndex]->genome,
                                          pop->species[speciesIndex]->organisms[fatherIndex]->genome,
                                          organisms[motherIndex]->fitness,
                                          pop->species[speciesIndex]->organisms[fatherIndex]->fitness);
                    if (motherIndex == fatherIndex)
                        bMutateWeights = true;
                }
                // Otherwise mate within the species
                else {
                    int fatherIndex = ceil(NEAT::GetRandomExp()*indOfGen)-1;
                    baby = GenomeMatching(organisms[motherIndex]->genome,
                                          organisms[fatherIndex]->genome,
                                          organisms[motherIndex]->fitness,
                                          organisms[fatherIndex]->fitness);
                    if (motherIndex == fatherIndex)
                        bMutateWeights = true;
                }
            }
            // Decide on weather to mutate weights or add connection/neuron of baby
            if (NEAT::chanceOfMutation || bMutateWeights){
                // Should we add a neuron?
                if (NEAT::ChanceOfEvent(NEAT::chanceOfAddingNeuron)){
                    baby->MutateAddNeuron(pop);
                }// Should we add a connection?
                else if (NEAT::ChanceOfEvent(NEAT::chanceOfAddingConnection)){
                    baby->MutateAddConnection(pop);
                } // Otherwise perform non-structural mutations
                else{
                    if (NEAT::ChanceOfEvent(NEAT::chanceOfMutation)){
                        precision weightUpdate=NEAT::dist(NEAT::rand)*2; //TODO powermod - Modified power by gene number, newer genes are modified more
                        //baby->MutateWeights();
                    }
                    if (NEAT::ChanceOfEvent(NEAT::chanceOfDisableConn)) {
                        baby->MutateDisableCon();
                    }
                    if (NEAT::ChanceOfEvent(NEAT::chanceOfEnableConn)) {
                        baby->MutateEnableCon();
                    }
                }
            }
        }

        // Add the baby to a species if it doesn't fit any create one
        pop->organisms.emplace_back(make_shared<Organism>(baby,pop->organisms.size()));

        // Find a compatible species for the baby
        for (int i=0;i<pop->species.size();i++){
            // Check if baby organism is compatible with this species
            if (pop->CheckGenomeDistance(pop->organisms.back(), pop->species[i]->organisms.front())) {
                pop->species[i]->organisms.emplace_back(pop->organisms.back());
                pop->organisms.back()->species = pop->species[i];
                break;
            }// If the baby organism is not compatible with any existing species create a new one
            else if (i == pop->species.size()-1){
                pop->AddSpecies(pop->organisms.back());
                break;
            }
        }
    }
    // Increase the age of the species after reproduce
    age++;
}

void Species::AdjustFitness(Population *pop){
    totalSpeciesFitness = 0;
    for (int i=0; i < organisms.size();i++){
        // Store original fitness
        organisms[i]->origFitness = organisms[i]->fitness;
        organisms[i]->fitness = organisms[i]->fitness/organisms.size();
        totalSpeciesFitness += organisms[i]->fitness;
    }
    avgFitness = totalSpeciesFitness/organisms.size();
    // Sort organisms in species in order of fitness
    sort(organisms.begin(), organisms.end(), NEAT::SortByFitness);

    // Update age of last improvement
    if (organisms[0]->origFitness > maxFitnessEver) {
        ageOfLastImprovement = age;
        maxFitnessEver = organisms[0]->origFitness;
    }

    // Kill off low performing organisms. These organisms will not be allowed to reproduce
    killIndex = floor(NEAT::survivalThreshold * organisms.size())+1;
    // Erase organisms from population list
    for(int j=killIndex; j<organisms.size();j++){
        pop->organisms.erase(pop->organisms.begin() + organisms[j]->populationIndex);
    }
    // Erase organisms from species list
    organisms.erase(organisms.begin()+killIndex,organisms.end());
}

shared_ptr<Genome> Species::GenomeMatching(shared_ptr<Genome> gen1, shared_ptr<Genome> gen2, float fitness1, float fitness2) {
    bool g1better = false;  // Is genome 1 fitness better?
    bool bUseInferior = NEAT::ChanceOfEvent(NEAT::chanceOfUseInferiorDisjoint);
    int gen1Size = gen1->localConnections.size();
    int gen2Size = gen2->localConnections.size();
    int i = 0;
    int j = 0;
    shared_ptr<Genome>  babyGenome = make_shared<Genome>();     // Genome of offspring
    shared_ptr<LConnection> chosenConnection;    // Selected connection from the gene pool

    //Figure out which genome is better
    //The worse genome should not be allowed to add extra structural baggage
    //If they are the same, use the smaller one's disjoint and excess genes only
    if (fitness1>fitness2)
        g1better=true;
    else if (fitness1==fitness2) {
        if (gen1->localConnections.size()<gen1->localConnections.size())
            g1better=true;
    }

    // Loop connections/genes of both genomes and add the relevant connections to the offspring
    while(i < gen1Size &&
          j < gen2Size) {
        bool bShouldDisable = false;
        precision averageWeight = 0;
        // If gen1 at end then add gen2 connection if it is fitter (i.e. skip excess connections of inferior genome)
        if (i==gen1Size) {
            if (g1better) break;
            chosenConnection = gen2->localConnections[j];
            j++;
        }
        // If gen2 at end then add gen1 connection if it is fitter (i.e. skip excess connections of inferior genome)
        else if (j==gen2Size) {
            if (!g1better) break;
            chosenConnection = gen1->localConnections[i];
            i++;
        }
        else {
            int innov1 = gen1->localConnections[i]->gConnection->innovNo;
            int innov2 = gen2->localConnections[j]->gConnection->innovNo;
            // If connections/genes match then randomly select a connection
            if (innov1 == innov2) {
                // Take average connection weight of parents
                if (NEAT::ChanceOfEvent(NEAT::chanceOfAverageConnWeight)){
                    averageWeight = gen2->localConnections[j]->weight+((gen1->localConnections[i]->weight - gen2->localConnections[j]->weight)/2);
                }
                // Matching connections so select one at random
                if (NEAT::ChanceOfEvent(0.5)){
                    chosenConnection = gen1->localConnections[i];
                }
                else{
                    chosenConnection = gen2->localConnections[j];
                }
                // If either connection is disabled then maybe disable this one
                if (!gen1->localConnections[i]->bIsEnabled || !gen2->localConnections[j]->bIsEnabled) {
                    if (NEAT::ChanceOfEvent(NEAT::chanceOfInheritDisabledConn)) bShouldDisable = true;
                }
                i++;
                j++;
            }
            // If the innov of org1 connection is less than org2 connection then increment disjoint and org1 index
            else if (innov1 < innov2) {
                // If gen1 is inferior and we shouldn't use inferior genome's disjoints then continue
                if (!g1better && !bUseInferior){
                    i++;
                    continue;
                }
                chosenConnection = gen1->localConnections[i];
                i++;
            }
                // Else org2 innov is less than org1 innov so increment disjoint and org2 index
            else {
                // If gen2 is inferior and we shouldn't use inferior genome's disjoints then continue
                if (g1better && !bUseInferior){
                    j++;
                    continue;
                }
                chosenConnection = gen2->localConnections[j];
                j++;
            }
        }
        if (chosenConnection) {
            // Add chosen connection to the baby's genome
            babyGenome->AddGene(chosenConnection);
            if (bShouldDisable)
                babyGenome->localConnections.back()->bIsEnabled = false;
            else
                babyGenome->localConnections.back()->bIsEnabled = true;
            // Make con weight average of parents if chanceOfAverageConnWeight
            if (averageWeight != 0)
                babyGenome->localConnections.back()->weight = averageWeight;
        }
    }

    return babyGenome;
}
