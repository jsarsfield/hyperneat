//
// Created by Joe on 23/12/2015.
//

#include <iostream>
#include "ConvNN.h"


// TODO Neural network needs to perform a task and calculate it's fitness (How well it performed the task)
void ConvNN::GetOutput(){
    precision input[] = {0.1,0.2,0.3,0.4,0.5,0.6,0.7};
    activations.push_back(vector<precision>(input, input + sizeof(input) / sizeof(precision)));
    for (int i = 1; i < org->genome->localLayers.size(); i++) {
        activations.push_back(vector<precision>());
        for (int j = 0; j < org->genome->localLayers[i]->localNeurons.size(); j++) {
            precision neuronActivation;
            for (int k = 0; k < org->genome->localLayers[i]->localNeurons[j]->localConnections.size(); k++){
                neuronActivation += activations[org->genome->localLayers[i]->localNeurons[j]->localConnections[k]->lIn->gNeuron->layer->depth][org->genome->localLayers[i]->localNeurons[j]->localConnections[k]->lIn->position] * org->genome->localLayers[i]->localNeurons[j]->localConnections[k]->weight;
            }
            activations[i].push_back(neuronActivation / (1 + abs(neuronActivation))); // Squashed with "fast sigmoid" (approximation of sig function)
        }
    }

    for (int i = 0; i < activations.back().size(); i++){
        cout << activations.back()[i] << " ";
    }
    cout << endl;

    CalculateFitness();
}

void ConvNN::CalculateFitness() {
    precision desiredOutput[activations.back().size()];
    precision error = 0;
    precision score = activations.back().size()*1;
    for (int i = 0; i < activations.back().size(); i++){
        desiredOutput[i] = 0.1;
    }
    for (int i = 0; i < activations.back().size(); i++){
        error += abs(desiredOutput[i] - activations.back()[i]);
    }
    error/activations.back().size();
    org->fitness = score-error;
}