//
// Created by joe on 06/06/15.
//

#ifndef NEAT_ORGANISM_H
#define NEAT_ORGANISM_H

#include "CustomDatatypes.h"

class Genome;
class Species;

class Organism {
public:
    Organism(std::shared_ptr<Genome> genome, int populationIndex) : genome(genome), populationIndex(populationIndex) {};
    Organism();

    std::shared_ptr<Genome> genome;     // Genome this organism uses
    std::shared_ptr<Species> species;   // Species this organism is in
    precision fitness;                      // Fitness of the organism
    precision origFitness;                  // Fitness of organism before adjustment
    int populationIndex;                    // Index of organism in organisms vector (Population.h) used to delete at end of epoch
};


#endif //NEAT_ORGANISM_H
