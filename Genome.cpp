//
// Created by joe on 01/06/15.
//

#include "Genome.h"
#include "Population.h"
#include <iostream>


/**************** Execution timer code ***************************
#include <chrono>
using namespace std::chrono;
high_resolution_clock::time_point t1 = high_resolution_clock::now();
high_resolution_clock::time_point t2 = high_resolution_clock::now();
auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
std::cout << "\nIt took: " << duration << "\n";
*/

Genome::Genome(){
}

Genome::~Genome(){

}

void Genome::AddLocalConnection(shared_ptr<GConnection> gConnection, precision weight, bool bIsEnabled){
    shared_ptr<LNeuron> lIn;
    shared_ptr<LNeuron> lOut;
    for (int i=0;i<localNeurons.size();i++){
        if (localNeurons[i]->gNeuron == gConnection->gIn){
            lIn = localNeurons[i];
            if (lIn && lOut)
                break;
        }
        else if (localNeurons[i]->gNeuron == gConnection->gOut){
            lOut = localNeurons[i];
            if (lIn && lOut)
                break;
        }
    }
    localConnections.emplace_back(make_shared<LConnection>(gConnection, lIn, lOut, weight, bIsEnabled));
    // Add the connection to the list of connections going into the local out neuron
    lOut->localConnections.emplace_back(localConnections.back());
}

void Genome::AddLocalNeuron(std::shared_ptr<GNeuron> gNeuron, bool bCheckIfExists) {
    // If neuron is already in genome then return
    if(bCheckIfExists){
        shared_ptr<LLayer> layer = GetLocalLayer(gNeuron->layer->depth);
        if (layer) {
            for (int i = 0; i < layer->localNeurons.size(); i++) {
                if (layer->localNeurons[i]->gNeuron == gNeuron) {
                    return;
                }
            }
        }
    }
    // Get size of local layer for neuron id and increase layer numOfNeurons
    shared_ptr<LLayer> tempLayer;
    int position;
    for (int i=0;i<localLayers.size();i++){
        if (localLayers[i]->gLayer == gNeuron->layer){
            position = localLayers[i]->numOfNeurons;
            localLayers[i]->numOfNeurons++;
            tempLayer = localLayers[i];
        }
    }
    localNeurons.emplace_back(make_shared<LNeuron>(gNeuron, position));
    tempLayer->localNeurons.emplace_back(localNeurons.back());
}

void Genome::AddLocalLayer(std::shared_ptr<GLayer> gLayer, bool bCheckIfExists) {
    // Check if layer exists in genome
    if (bCheckIfExists) {
        for (int i = 0; i < localLayers.size(); i++) {
            if (localLayers[i]->gLayer == gLayer)
                return;
        }
    }
    // If not then add the layer
    localLayers.emplace_back(make_shared<LLayer>(gLayer));
}

void Genome::AddGene(shared_ptr<LConnection> lConnection){
    /*
    bool bInLayer, bOutLayer = false;   // Do we already have the in neuron layer or out neuron layer?
    bool bInNeuron, bOutNeuron = false;   // Do we already have the in neuron or out neuron?
    int in,out; // indicies of local in and out neurons
    // Add layer if not exists
    for (int i=0;i<localLayers.size();i++){
        if (localLayers[i]->gLayer == lConnection->lIn->gNeuron->layer){
            if (bOutLayer) break;
            else bInLayer = true;
        }
        else if (localLayers[i]->gLayer == lConnection->lOut->gNeuron->layer) {
            if (bInLayer) break;
            else bOutLayer = true;
        }
        // Add the layer/s if we get to the end and they don't exist
        if (i == localLayers.size()-1){
            if (!bInLayer)
                AddLocalLayer(lConnection->lIn->gNeuron->layer);
                //localLayers.emplace_back(make_shared<LLayer>(lConnection->lIn->gNeuron->layer));
            if (!bOutLayer)
                AddLocalLayer(lConnection->lOut->gNeuron->layer);
                //localLayers.emplace_back(make_shared<LLayer>(lConnection->lOut->gNeuron->layer));
        }
    }
    // Add neurons if not exists
    for (int i=0;i<localNeurons.size();i++) {
        if (localNeurons[i]->gNeuron == lConnection->lIn->gNeuron){
            in = i;
            if (bOutNeuron) break;
            else bInNeuron = true;
        }
        else if (localNeurons[i]->gNeuron == lConnection->lOut->gNeuron) {
            out = i;
            if (bInNeuron) break;
            else bOutNeuron = true;
        }
        // Add the neuron/s if we get to the end and they don't exist
        if (i == localNeurons.size()-1){
            if (!bInNeuron) {
                // Update number of neurons in layer and give the local neuron an id based off the number of neurons in layer
                AddLocalNeuron(lConnection->lIn->gNeuron);
            }
            if (!bOutNeuron) {
                // Update number of neurons in layer and give the local neuron an id based off the number of neurons in layer
                AddLocalNeuron(lConnection->lIn->gNeuron);
            }
        }
    }*/
    // Add layers
    AddLocalLayer(lConnection->lIn->gNeuron->layer,true);
    AddLocalLayer(lConnection->lOut->gNeuron->layer,true);
    // Add neurons
    AddLocalNeuron(lConnection->lIn->gNeuron, true);
    AddLocalNeuron(lConnection->lOut->gNeuron, true);
    // Add connection
    AddLocalConnection(lConnection->gConnection, lConnection->weight, lConnection->bIsEnabled);
    //localConnections.emplace_back(make_shared<LConnection>(lConnection->gConnection, localNeurons[in], localNeurons[out], lConnection->weight, lConnection->bIsEnabled));
}

shared_ptr<Genome> Genome::Clone(){
    shared_ptr<Genome> copy = make_shared<Genome>();
    // Copy layers
    for (int i=0;i<localLayers.size();i++){
        copy->AddLocalLayer(localLayers[i]->gLayer, false);
        //copy->localLayers.emplace_back(make_shared<LLayer>(localLayers[i]->gLayer));
    }
    // Copy neurons
    for (int i=0;i<localNeurons.size();i++){
        copy->AddLocalNeuron(localNeurons[i]->gNeuron, false);
        //copy->localNeurons.emplace_back(make_shared<LNeuron>(localNeurons[i]->gNeuron, localNeurons[i]->position));
    }
    // Copy connections
    for (int i=0;i<localConnections.size();i++){
        /*
        std::shared_ptr<LNeuron> lIn;
        std::shared_ptr<LNeuron> lOut;
        // Find local in neuron and local out neuron for the new local connection
        for (int i=0; i<copy->localNeurons.size();i++){
            if (copy->localNeurons[i] == localConnections[i]->lIn &&
                    copy->localNeurons[i]->gNeuron->layer == localConnections[i]->lIn->gNeuron->layer){
                lIn = copy->localNeurons[i];
                if (lOut) break; // If lOut is not null then break
            }
            else if (copy->localNeurons[i] == localConnections[i]->lOut &&
                     copy->localNeurons[i]->gNeuron->layer == localConnections[i]->lOut->gNeuron->layer){
                lOut = copy->localNeurons[i];
                if (lIn) break; // If lIn is not null then break
            }
        }
        copy->localConnections.emplace_back(make_shared<LConnection>(localConnections[i]->gConnection, lIn, lOut, localConnections[i]->weight, localConnections[i]->bIsEnabled));
         */
        copy->AddLocalConnection(localConnections[i]->gConnection,localConnections[i]->weight,localConnections[i]->bIsEnabled);
    }
    return copy;
}

void Genome::MutateWeights(double power, double rate, Mutator mutator) {
    bool bIsSevere = NEAT::ChanceOfEvent(NEAT::chanceOfRandomWeight);   // Change weights to random value if severe
    precision mutateDivision = 0.8/(localConnections.size()-1); //The multiplication of mutation will rise farther into the genome on the theory that the older genes are more fit since they have stood the test of time

    // Loop connections and mutate the weights
    for (int i=0;i<localConnections.size();i++){
        // Only mutate weights that are enabled
        if(localConnections[i]->bIsEnabled) {
            // Are we severely changing the weights i.e. set weights to random number or subtle adjustments
            if(bIsSevere){
                localConnections[i]->weight = NEAT::weightDistr(NEAT::rand);
            }
            else{
                localConnections[i]->weight += (NEAT::gaussDistri(NEAT::rand)*(0.2+(i*mutateDivision)));
            }
        }
    }
}

void Genome::MutateAddNeuron(Population *pop){
    shared_ptr<GNeuron> newNeuron;
    shared_ptr<GConnection> newCon1;
    shared_ptr<GConnection> newCon2;
    // Select a random connection index weighted towards older connections
    int randomConIndex = ceil(NEAT::GetRandomExp()*localConnections.size())-1;
    // Get depth of new neuron
    float depth = (localConnections[randomConIndex]->lIn->gNeuron->layer->depth + localConnections[randomConIndex]->lOut->gNeuron->layer->depth)/2;
    // Disable old connection
    localConnections[randomConIndex]->bIsEnabled = false;
    std::tie(newNeuron, newCon1, newCon2) = pop->AddInnovationNeuron(localConnections[randomConIndex]->gConnection,GetLocalLayer(depth),depth);
    AddLocalLayer(newNeuron->layer, true);
    AddLocalNeuron(newNeuron, false);   // bCheck
    AddLocalConnection(newCon1,NEAT::weightDistr(NEAT::rand), 1);
    AddLocalConnection(newCon2,NEAT::weightDistr(NEAT::rand), 1);
}

void Genome::MutateAddConnection(Population *pop){
    // Sort layers by depth input layer first
    sort(localLayers.begin(),localLayers.end(),NEAT::SortLayersByDepth);
    vector<int> layersToCheck;    // Layers to check for potential connection excludes input layer
    shared_ptr<LNeuron> lIn;    // Local in neuron that can send a connection
    shared_ptr<LNeuron> lOut;   // Local out neuron that can receive a connection
    int layerOutInd;            // Index of layer of lOut neuron
    // Populate layersToCheck
    for(int i=1;i<localLayers.size();i++)
        layersToCheck.push_back(i);
    // Randomly loop layers until we find a neuron that can receive a connection
    for (int i=0;i<localLayers.size()-1;i++){
        int lInd2 = ceil(NEAT::dist(NEAT::rand)*layersToCheck.size())-1;
        int lInd = layersToCheck[lInd2];
        vector<int> neuronsToCheck;
        for (int j=0;j<localLayers[lInd]->localNeurons.size();j++)
            neuronsToCheck.push_back(j);
        // Randomly loop neurons in layer until we find a neuron that can receive a connection
        for (int j=0;j<localLayers[lInd]->localNeurons.size();j++){
            int nInd2 = ceil(NEAT::dist(NEAT::rand)*neuronsToCheck.size())-1;
            int nInd = neuronsToCheck[nInd2];
            int numberOfNeuronsAtLowerDepth = 0;
            // Loop layers at lower depth and sum number of neurons to see if it is larger than connections going into the randomly selected neuron. If it is then there is a potential connection available
            for (int k=0;k<lInd;k++){
                numberOfNeuronsAtLowerDepth += localLayers[k]->localNeurons.size();
                if (numberOfNeuronsAtLowerDepth > localLayers[lInd]->localNeurons[nInd]->localConnections.size()){
                    lOut = localLayers[lInd]->localNeurons[nInd];
                    layerOutInd = lInd;
                    break;
                }
            }
            // If we found an out neuron then break and look for in neuron
            if (lOut)
                break;
            // Remove neuron that was checked
            neuronsToCheck.erase(neuronsToCheck.begin()+nInd2);
        }
        // If we found an out neuron then break and look for in neuron
        if (lOut)
            break;
        // If we have checked all non input neurons and they are all fully connected then no new connection can be added so return
        if (i == localLayers.size()-2)
            return;
        // Remove layer that was checked
        layersToCheck.erase(layersToCheck.begin()+lInd2);
    }
    // Now look for lIn neuron that doesn't have a connection to lOut
    vector<int> layersBefore;    // Layers to check for available input neuron
    for(int i=0;i<layerOutInd;i++)
        layersBefore.push_back(i);
    for (int i=0; i<layerOutInd;i++) {
        int lInd2 = ceil(NEAT::dist(NEAT::rand)*layersBefore.size())-1;
        int lInd = layersBefore[lInd2];
        vector<int> neuronsToCheck;
        for (int j=0;j<localLayers[lInd]->localNeurons.size();j++)
            neuronsToCheck.push_back(j);
        for (int j=0; j<localLayers[lInd]->localNeurons.size();j++) {
            int nInd2 = ceil(NEAT::dist(NEAT::rand)*neuronsToCheck.size())-1;
            int nInd = neuronsToCheck[nInd2];
            // Check if the selected local out neuron already has a connection to this randomly selected local in neuron
            for (int k=0;k<lOut->localConnections.size();k++){
                if(lOut->localConnections[k]->lIn == localLayers[lInd]->localNeurons[nInd]) // Choose another neuron to check if there is already a connection to this one
                    break;
                else if (k == lOut->localConnections.size()-1){ // Use this neuron at lIn as it doesn't have an existing connection to lOut
                    lIn = localLayers[lInd]->localNeurons[nInd];
                    break;
                }
            }
            if (lIn)
                break;
            // Remove neuron that was checked
            neuronsToCheck.erase(neuronsToCheck.begin()+nInd2);
        }
        if (lIn)
            break;
        // Remove layer that was checked
        layersBefore.erase(layersBefore.begin()+lInd2);
    }
    if (!lIn || !lOut){
        int aa=1;
    }
    // Add the new connection
    AddLocalConnection(pop->AddInnovationConnection(lIn->gNeuron, lOut->gNeuron), NEAT::weightDistr(NEAT::rand), 1);
}

void Genome::MutateEnableCon(){

    //for(int i=localConnections-1;i>=0;i--){

   // }
}

void Genome::MutateDisableCon(){

}

std::shared_ptr<LLayer> Genome::GetLocalLayer(float depth){
    for (int i=0;i<localLayers.size();i++){
        if (localLayers[i]->gLayer->depth == depth){
            return localLayers[i];
        }
    }
    return nullptr;
}