//
// Created by joe on 01/06/15.
//

#ifndef NEAT_CUSTOMDATATYPES_H
#define NEAT_CUSTOMDATATYPES_H

#include <memory>
#include <vector>

class Genome;
class Population;

typedef float precision;    // Easily switch precision from float (32 bit) to double (64 bit)

using namespace std;

struct GNeuron; //Forward declare
// Global layers generated from the beginning of the population
struct GLayer{
    float depth;          // Depth of layer. 0 = input; 1 = output; > 0 && < 1 = hidden;
    //std::vector<std::shared_ptr<GNeuron>> neuronsInLayer;
    GLayer(int _depth) : depth(_depth){};
    GLayer(){};

    template <class Archive>
    void serialize(Archive & ar){
        ar(depth);
    }
};
// Global neurons generated from the beginning of the population
struct GNeuron{
    std::shared_ptr<GLayer> layer;   // Global layer of neuron
    GNeuron(std::shared_ptr<GLayer> _layer) : layer(_layer){};
    GNeuron(){};

    template <class Archive>
    void serialize(Archive & ar){
        ar(layer);
    }
};
struct InnovationAddNeuron; // Forward declare
// Global connections generated from the beginning of the population
struct GConnection {
    std::shared_ptr<GNeuron> gIn;   // Global neuron going into connection
    std::shared_ptr<GNeuron> gOut;  // Global neuron going out of connection
    int innovNo;                    // Unique innovation number used for matching genomes during reproduction
    vector<std::shared_ptr<InnovationAddNeuron>> innovsFromCon;  // Add neuron innovations that originate from this con (used to check if innov exists in network)
    GConnection(std::shared_ptr<GNeuron> gIn, std::shared_ptr<GNeuron> gOut) : gIn(gIn), gOut(gOut) {};
    GConnection(){};

    template <class Archive>
    void serialize(Archive & ar){
        ar(gIn, gOut, innovNo, innovsFromCon);
    }
};
struct LConnection; //Forward declare LConnection for localConnections list in LNeuron
// Local neuron data specific to the genome e.g. neuron state
struct LNeuron {
    std::shared_ptr<GNeuron> gNeuron;   // Global neuron this local neuron refers too
    std::vector<std::shared_ptr<LConnection>> localConnections;  // Array of local connections going into this neuron (Easier for neural net to calculate activations)
    int position;
    LNeuron(std::shared_ptr<GNeuron> gNeuron, int position) : gNeuron(gNeuron), position(position) {};
    LNeuron(){};

    template <class Archive>
    void serialize(Archive & ar){
        ar(gNeuron, localConnections, position);
    }
};
// Local connection data specific to the genome e.g. connection weight
struct LConnection {
    std::shared_ptr<GConnection> gConnection; // Global connection this local connection uses
    std::shared_ptr<LNeuron> lIn;             // Local neuron going into connection
    std::shared_ptr<LNeuron> lOut;            // Local neuron going out of connection
    precision weight;         // Weight of connection
    bool bIsEnabled;    // Is the connection enabled
    LConnection(std::shared_ptr<GConnection> gConnection, std::shared_ptr<LNeuron> lIn, std::shared_ptr<LNeuron> lOut, precision weight, bool bIsEnabled) : gConnection(gConnection), lIn(lIn), lOut(lOut), weight(weight), bIsEnabled(bIsEnabled){};
    LConnection(){};

    template <class Archive>
    void serialize(Archive & ar){
        ar(gConnection, lIn, lOut, weight, bIsEnabled);
    }
};
// Local layer data specific to the genome e.g. number of neurons in layer
struct LLayer {
    std::shared_ptr<GLayer> gLayer;
    std::vector<std::shared_ptr<LNeuron>> localNeurons;  // Array of local neurons in this layer (Easier for neural net to calculate activations)
    int numOfNeurons = 0;   // Number of neurons in the layer
    LLayer(std::shared_ptr<GLayer> gLayer) : gLayer(gLayer){};
    LLayer(){};

    template <class Archive>
    void serialize(Archive & ar){
        ar(gLayer, localNeurons, numOfNeurons);
    }
};

// Innovations used to track innovations in the population (new connection/new neuron)
// to ensure organisms use same innovations during mutation.
// This allows for an accurate distance measure to speciate the population based on similarity
struct InnovationAddCon {
    shared_ptr<GNeuron> inNeuron;   // Existing in neuron where connection begins
    shared_ptr<GNeuron> outNeuron;  // Existing out neuron where connection ends
    shared_ptr<GConnection> newConnection;  // New con that joins the in/out neurons

    InnovationAddCon(shared_ptr<GConnection> newConnection, shared_ptr<GNeuron> inNeuron, shared_ptr<GNeuron> outNeuron) : newConnection(newConnection), inNeuron(inNeuron),  outNeuron(outNeuron) {};

    template <class Archive>
    void serialize(Archive & ar){
        ar(inNeuron,outNeuron,newConnection);
    }
};
struct InnovationAddNeuron {
    //shared_ptr<GConnection> originatedFrom;   // Connection this innovation came from
    shared_ptr<GConnection> newCon1;  // New connection created by this innovation
    shared_ptr<GConnection> newCon2;  // New connection created by this innovation
    shared_ptr<GNeuron> newNeuron;  // New neuron created by this innovation

    InnovationAddNeuron(shared_ptr<GNeuron> newNeuron, shared_ptr<GConnection> newCon1, shared_ptr<GConnection> newCon2) : newNeuron(newNeuron),newCon1(newCon1),newCon2(newCon2) {};
    InnovationAddNeuron(){};
    template <class Archive>
    void serialize(Archive & ar){
        ar(newNeuron,newCon1,newCon2);
    }
};

enum Mutator {
    GAUSSIAN = 0,
    COLDGAUSSIAN = 1
};

typedef std::shared_ptr<Genome> GenomeShared;


#endif //NEAT_CUSTOMDATATYPES_H
