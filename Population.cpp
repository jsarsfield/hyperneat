//
// Created by joe on 31/05/15.
//

#include <cmath>
#include <string>
#include <ctime>
#include "Population.h"
#include "Species.h"
#include "Organism.h"
#include "Genome.h"
#include "ConvNN.h"
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>
#include <fstream>


using namespace std;

namespace NEAT {
    precision disjointCoeff = 2;            // Coefficient for disjoint connections/genes
    precision excessCoeff = 1;              // Coefficient for excess connections/genes
    precision avgWeighDiffCoeff = 2;        // Coefficient for average weight differences for matching connections/genes
    precision distanceThreshold = 2;        // Dynamic variable to control desired num of species
    int populationSize = 0;                // Size of population to generate for new generation
    int minAgeToKillSpecies = 20;           // The minimum age a species must be before it can be killed off, gives young species a chance of improving
    precision interspeciesMateRate = 0.02;      // Chance of organism mating with another organism outside of it's species
    precision chanceOfMutation = 0.2;           // Chance of genome being mutated either structurally or connection weights
    precision chanceOfRandomWeight = 0.25;      // Chance of being assigned a completely random weight as opposed to uniform/gaussian mutation
    precision chanceOfInheritDisabledConn = 0.75; // Chance that inherited gene is disabled if it is disabled in either parent (during crossover)
    precision chanceOfEnableConn = 0.1;         // Chance of enabling connection
    precision chanceOfDisableConn = 0.1;        // Chance of disabling connection
    precision chanceOfAverageConnWeight = 0.3;  // Chance of inherited gene taking the average conn weight of both parent genes as opposed to 1 parent randomly (during crossover)
    precision chanceOfAddingNeuron = 0.01;      //0.01 Chance of adding a Neuron to an offspring's genome (during crossover)
    precision chanceOfAddingConnection = 0.1;   //0.1 Chance of adding a connection to an offspring's genome (during crossover)
    precision chanceOfCrossover = 0.75;         // Chance of crossover occurring. If crossover doesn't occur then weights may be mutated.
    precision survivalThreshold = 0.4;          // Percent of average fitness for survival in species
    precision chanceOfInterspeciesMate = 0.01;  // Chance of selecting a mate from another species
    precision chanceOfUseInferiorDisjoint = 0.05;   // Chance of using inferior genome disjoints during crossover
    precision percentOfPopulationChamps = 0.2;  // Percentage of total pop to reproduce population champ in next generation
    std::mt19937 rand;                          // Pseudo random number generator seeded random_device in constructor
    uniform_real_distribution<precision> dist;  // Uniform distribution
    exponential_distribution<precision> expoDistri(3.5); // exponential distribution favouring numbers tending to 0
    uniform_real_distribution<precision> weightDistr(0.3,2);  // Random weight distribution
    normal_distribution<precision> gaussDistri(0,0.4);  // Gaussian distribution favouring smaller values near 0 for weight changes

    void SetupRand(){
        random_device rd;  // Required to seed the random number generator
        //rand.seed(rd());
        rand.seed( time(NULL) );
    }

    precision GetRandomExp(){
        precision val = NEAT::expoDistri(rand);
        return (val>1) ? 1:val;
    }

    bool ChanceOfEvent(precision event){
        return (event > dist(rand) ? true : false);
    }
    bool SortByFitness(std::shared_ptr<Organism> o1, std::shared_ptr<Organism> o2 ){ return (o1->fitness > o2->fitness); } // Sort organisms by fitness, higher values = more fit
    bool SortByInnovNum(std::shared_ptr<LConnection> c1, std::shared_ptr<LConnection> c2) { return (c1->gConnection->innovNo < c2->gConnection->innovNo); }       // Sort local connections by innovation number
    bool SortLayersByDepth(std::shared_ptr<LLayer> l1, std::shared_ptr<LLayer> l2) { return (l1->gLayer->depth < l2->gLayer->depth); }       // Sort layers by depth
}

bool SortSpeciesByAvgFitness(std::shared_ptr<Species> s1, std::shared_ptr<Species> s2 ){ return (s1->avgFitness > s2->avgFitness); } // Sort species by adjusted avgFitness, higher values = more fit
bool SortOrganismsByMaxFitness(std::shared_ptr<Organism> o1, std::shared_ptr<Organism> o2 ){ return (o1->fitness > o2->fitness); } // Sort organisms by original max fitness, higher values = more fit
bool SortSpeciesByMaxFitness(std::shared_ptr<Species> s1, std::shared_ptr<Species> s2 ){ return (s1->maxFitness > s2->maxFitness); } // Sort species by original max fitness, higher values = more fit


/* CLASS Population
 * Holds the species within the pop
 * */

// Constructor when genomes are loaded from file
Population::Population(string genomesFile) {
    NEAT::SetupRand();
    LoadInitialPopulation(genomesFile);
}
// Constructor when x number of minimalist topologies are created
Population::Population(int numOfInputNeurons, int numOfOutputNeurons, int numOfGenomes) {
    NEAT::SetupRand();
    NEAT::populationSize = numOfGenomes;
    LoadInitialPopulation("", numOfInputNeurons, numOfOutputNeurons, numOfGenomes);
}

std::vector<int> Population::DistributeConnections(int numOfInputNeurons, int numOfOutputNeurons){
    vector<int> set(numOfOutputNeurons, 1);
    int assigned = numOfOutputNeurons;
    int i = 0;
    while (assigned < numOfInputNeurons){
        set[i] += 1;
        assigned++;
        if (i == numOfOutputNeurons-1)
            i=0;
        else
            i++;
    }
    return set;
}

void Population::LoadInitialPopulation(std::string genomesFile, int numOfInputNeurons, int numOfOutputNeurons, int numOfGenomes) {
    // If no genomes file then create new genomes with minimal connections that satisfies at least one connection for each neuron
    if (genomesFile.empty()) {
        // Determine minimum amount of connections each output neuron requires and place in vector. This guarantees each neuron has at least one connection.
        vector<int> set = DistributeConnections(numOfInputNeurons, numOfOutputNeurons);
        srand(unsigned(std::time(0)));
        // Index of input neurons that require a connection. If all input neurons have a connection but there is still output neurons without connections then this vector is repopulated
        std::vector<int> needsConnection;
        // Ensure only one lot of global neurons are created
        for (int j = 0; j < numOfInputNeurons; j++) {
            AddGlobalNeuron(0);
        }
        for (int j = 0; j < numOfOutputNeurons; j++) {
            AddGlobalNeuron(1);
        }
        // Add global neuron pointers to genome then create global connections and add to genome
        for (int i = 0; i < numOfGenomes; i++) {
            random_shuffle(set.begin(),set.end());
            genomes.emplace_back(make_shared<Genome>(Genome()));
            // Add local layers
            for (int j = 0; j < globalLayers.size(); j++) {
                genomes.back()->AddLocalLayer(globalLayers[j], false);
            }
            // Add pointers to global neurons to the genome
            for (int j = 0; j < globalNeurons.size(); j++) {
                genomes.back()->AddLocalNeuron(globalNeurons[j], false);
            }
            // Clear needsConnection for new genome
            needsConnection.clear();
            // Add connections
            for (int j=0; j<set.size(); j++) {
                for (int k=0; k<set[j]; k++) {
                    if (needsConnection.size() == 0){
                        for (int l = 0; l < numOfInputNeurons; l++) {
                            needsConnection.push_back(l);
                        }
                    }
                    uniform_int_distribution<int> ncDistri(0,needsConnection.size()-1);
                    int index = ncDistri(NEAT::rand);
                    genomes.back()->AddLocalConnection(AddInnovationConnection(globalNeurons[needsConnection[index]], globalNeurons[numOfInputNeurons+j]), NEAT::weightDistr(NEAT::rand), 1);
                    needsConnection.erase(needsConnection.begin()+index);
                }
            }
        }
        // TODO check if we need to sort connections in the Speciate() function OTHERWISE ensure crossover generates genomes with connection orders with innov number ascending
        // Loop each genome and sort connection innovation numbers in ascending order
        for (int i=0; i<genomes.size();i++){
            sort(genomes[i]->localConnections.begin(), genomes[i]->localConnections.end(), NEAT::SortByInnovNum);
        }
        generation = 0;
        SaveGenomes("genomes.bin");
    }
    // Reconstruct genomes vector from serialized file
    else{
        LoadGenomes(genomesFile);
    }
    // Shuffle genomes
    random_shuffle(genomes.begin(), genomes.end());
    // Loop each genome and create an organism
    for (int i=0;i<genomes.size();i++) {
        organisms.emplace_back(make_shared<Organism>(genomes[i],organisms.size()));
    }
    InitialSpeciate();
}

bool Population::CheckGenomeDistance(shared_ptr<Organism> org1, shared_ptr<Organism> org2) {
    int org1Size = org1->genome->localConnections.size();
    int org2Size = org2->genome->localConnections.size();
    int scale = (org1Size > org2Size ? org1Size : org2Size);    //  Scale number of excess and disjoint connections by size of largest genome
    int weightScale = 0;    // Scale weight differences by number of matching connections
    int excess = 0;
    int disjoint = 0;
    precision avgWeightDifference = 0;    // Average weight difference between matching connections/genes
    int i,j = 0;

    // Loop connections of both organisms calculating the disjoint,excess and weight differences
    while(i < org1Size &&
            j < org2Size) {
        // If org1 is at the end then increment excess and org2 index
        if (i==org1Size) {
            j++;
            excess++;
        }
        // If org2 is at the end then increment excess and org1 index
        else if (j==org2Size) {
            i++;
            excess++;
        }
        else {
            int innov1 = org1->genome->localConnections[i]->gConnection->innovNo;
            int innov2 = org2->genome->localConnections[j]->gConnection->innovNo;
            // If connections/genes match then calculate weight difference
            if (innov1 == innov2) {
                weightScale+=1.0;
                avgWeightDifference += abs(org1->genome->localConnections[i]->weight - org2->genome->localConnections[j]->weight);
                i++;
                j++;
            }
            // If the innov of org1 connection is less than org2 connection then increment disjoint and org1 index
            else if (innov1 < innov2) {
                disjoint++;
                i++;
            }
            // Else org2 innov is less than org1 innov so increment disjoint and org2 index
            else {
                disjoint++;
                j++;
            }
        }
    }

    return (NEAT::disjointCoeff*(disjoint/scale)+
            NEAT::excessCoeff*(excess/scale)+
            NEAT::avgWeighDiffCoeff*(avgWeightDifference/weightScale) < NEAT::distanceThreshold);

}

void Population::AddSpecies(shared_ptr<Organism> organism){
    species.emplace_back(make_shared<Species>(numberOfSpecies, generation));
    species.back()->organisms.emplace_back(organism);
    organism->species = species.back();
    numberOfSpecies++;
}

void Population::AssignInnovationNumber(shared_ptr<GConnection> connection){
    connection->innovNo = globalInnovationNumber;
    globalInnovationNumber++;
}

void Population::InitialSpeciate() {
    // Create first species and add first organism to it
    AddSpecies(organisms[0]);
    // Add organisms to a species
    for (int i=1;i<organisms.size();i++) {
        for (int j=0;j<species.size();j++) {
            if (CheckGenomeDistance(organisms[i], species[j]->organisms.front())) {
                species[j]->organisms.emplace_back(organisms[i]);
                organisms[i]->species = species[j];
                break;
            }
            else if (j == species.size()-1) {
                AddSpecies(organisms[i]);
                break;
            }
        }
    }

    for (int j=0;j<species.size();j++) {
        cout << species[j]->organisms.size() << endl;
    }
    Update();
}

void Population::Update(){
    // Main loop
    while(true){
        Epoch();
    }
}

void Population::Epoch(){
    cout << "Start of generation: " << generation <<  " size: " << organisms.size() << endl;
    precision totalPopFitness = 0;  // total of the fitness of all orgs in the population
    precision generationAverage;  //The average modified fitness among ALL organisms
    precision totalAverageSpecies = 0; // The total of average fitness for all species

    // Process organisms on a given task to determine fitness TODO multithread this
    for (int j=0;j<species.size();j++) {
        for (int i = 0; i < species[j]->organisms.size(); i++) {
            new ConvNN(species[j]->organisms[i]);
        }
        // TODO process fittest in species when this is multithreaded
    }

    // Every 30 generations Kill off weakest species older than minAgeToKillSpecies (COMPETITIVE COEVOLUTION) strongest species is exempt
    if (generation%30==0) {
        //Sort the Species by original max fitness
        for (int i=0;i<species.size();i++){
            sort(species[i]->organisms.begin(), species[i]->organisms.end(),SortOrganismsByMaxFitness);
            species[i]->maxFitness = species[i]->organisms[0]->fitness;
        }
        sort(species.begin(),species.end(), SortSpeciesByMaxFitness);
        for(int i=species.size()-1;i>0;i--){
            if (species[i]->age >= NEAT::minAgeToKillSpecies){
                species.erase(species.begin()+i);
                break;
            }
        }

    }

    // Once all organisms have completed the task and have a fitness reproduce next generation
    // TODO reproduce as quickly as possible and proccess info on current generation after new generation has been sent to threads for processing
    // TODO copy current organism generation to temp for saving to file once new generation of orgs have been sent off to their threads for processing
    /*
    NEAT::percentOfPopulationChamps*NEAT::populationSize;
    for (int j=0;j<species.size();j++) {
        cout << "Species: " << j << endl;
        for (int i = 0; i < species[j]->organisms.size(); i++) {
            cout << species[j]->organisms[i]->fitness << endl;
        }
        cout << endl;
    }*/
    
    // Adjust fitness of organisms in each species
    for (int i=0;i<species.size();i++){
        species[i]->AdjustFitness(this);
        totalAverageSpecies += species[i]->avgFitness;
    }
    // Organise species by average fitness
    sort(species.begin(), species.end(), SortSpeciesByAvgFitness);
    // Calculate number of organisms each species can produce
    int totalExpected = 0;
    for (int i=0;i<species.size();i++){
        species[i]->expectedOffspring = floor((species[i]->avgFitness/totalAverageSpecies)*NEAT::populationSize);
        if (species[i]->expectedOffspring == 0)
            species[i]->expectedOffspring = 1;
        totalExpected += species[i]->expectedOffspring;
        // Get index of last organism in this species generation
        species[i]->indOfGen = species[i]->organisms.size();
    }
    // If totalExpected is less than populationSize then add remainder to fittest species
    species[0]->expectedOffspring += NEAT::populationSize-totalExpected;

    // Reproduce next generation
    for(int i=0;i<species.size();i++){
        species[i]->Reproduce(this);
    }

    // Kill organisms from this generation
    for (int i=0; i<species.size();i++){
        if (species[i]->indOfGen > 0) {
            // Erase organisms from population list
            for(int j=0; j<species[i]->indOfGen;j++){
                organisms.erase(organisms.begin() + species[i]->organisms[j]->populationIndex);
            }
            // Erase organisms from species list
            species[i]->organisms.erase(species[i]->organisms.begin(),
                                        species[i]->organisms.begin() + species[i]->indOfGen);
        }
    }

    // Kill empty species
    for (int i=0; i<species.size();i++){
        if (species[i]->organisms.size() == 0){
            species.erase(species.begin()+i);
            i--;
        }
    }
    /*
    int speciesOrgTotal = 0;
    for (int i=0; i<species.size();i++){
        speciesOrgTotal += species[i]->organisms.size();
    }
    cout << "End of generation: " << generation << " " << speciesOrgTotal << " " << organisms.size() << endl;
     */
    cout << "End of generation: " << generation << endl;
    generation++;
}

void Population::SaveGenomes(string saveFile) {
    std::ofstream os(saveFile, std::ios::binary);
    {
        cereal::BinaryOutputArchive ar(os);
        ar << globalConnections << globalNeurons << globalLayers << globalInnovationNumber << genomes;
        os.close();
    }
}

void Population::LoadGenomes(string loadFile){
    std::ifstream is(loadFile, std::ios::binary);
    {
        cereal::BinaryInputArchive ar(is);
        ar >> globalConnections >> globalNeurons >> globalLayers >> globalInnovationNumber >> genomes;
    }
}

tuple<bool, shared_ptr<GConnection>> Population::AddGlobalConnection(std::shared_ptr<GNeuron> in, std::shared_ptr<GNeuron> out) {
    // Check if connection exists
    std::experimental::optional<shared_ptr<GConnection>> tc = DoesConnectionExist(in, out);
    if (tc){
        return make_tuple(false, tc.value());
    }
    // Otherwise add it
    else {
        globalConnections.emplace_back(make_shared<GConnection>(in, out));
        AssignInnovationNumber(globalConnections.back());
        return make_tuple(true, globalConnections.back());
    }
}

shared_ptr<GNeuron> Population::AddGlobalNeuron(float depth) {
    shared_ptr<GLayer> layer = AddGlobalLayer(depth);
    globalNeurons.emplace_back(make_shared<GNeuron>(layer));
    //layer->neuronsInLayer.emplace_back(globalNeurons.back());
    return globalNeurons.back();
}

shared_ptr<GLayer> Population::AddGlobalLayer(float depth){
    // Check if layer exists
    std::experimental::optional<shared_ptr<GLayer>> tl = DoesLayerExist(depth);
    if (bool(tl)) {
        return tl.value();
    }
    // Otherwise add it
    else{
        globalLayers.emplace_back(make_shared<GLayer>(depth));
        return globalLayers.back();
    }
}

experimental::optional<shared_ptr<GConnection>> Population::DoesConnectionExist(std::shared_ptr<GNeuron> in, std::shared_ptr<GNeuron> out) {
    for (int i=0;i<globalConnections.size();i++) {
        if (globalConnections[i]->gIn.get() == in.get() && globalConnections[i]->gOut.get() == out.get()){
            return experimental::make_optional(globalConnections[i]);
        }
    }
    // Return null
    return {};
}

experimental::optional<shared_ptr<GLayer>> Population::DoesLayerExist(float depth) {
    for (int i=0;i<globalLayers.size();i++){
        if (globalLayers[i]->depth == depth){
            return experimental::make_optional(globalLayers[i]);
        }
    }
    // Return null
    return {};
}

shared_ptr<GConnection> Population::AddInnovationConnection(std::shared_ptr<GNeuron> in, std::shared_ptr<GNeuron> out){
    bool bIsInnovation;
    shared_ptr<GConnection> gCon;
    tie(bIsInnovation, gCon) = AddGlobalConnection(in, out);
    // If the innovation is new add it
    if (bIsInnovation) {
        innovationsAddCon.emplace_back(make_shared<InnovationAddCon>(globalConnections.back(), in, out));
    }
    return gCon;
}
tuple<shared_ptr<GNeuron>,shared_ptr<GConnection>,shared_ptr<GConnection>> Population::AddInnovationNeuron(std::shared_ptr<GConnection> con, shared_ptr<LLayer> layer, float depth){
    // If layer is nullptr then use first innovation from global connection
    if(!layer && con->innovsFromCon.size() > 0){
        return make_tuple(con->innovsFromCon.front()->newNeuron,con->innovsFromCon.front()->newCon1,con->innovsFromCon.front()->newCon2);
    }
    // Look for existing innovation that is not used in this genome
    for (int i=0;i<con->innovsFromCon.size();i++){
        bool bNotFound = true;
        for (int j=0;j<layer->localNeurons.size();j++) {
            // If we found the newNeuron from this innovation in the local neurons then go onto the next innovation as it already exists in this genome
            if (con->innovsFromCon[i]->newNeuron == layer->localNeurons[j]->gNeuron){
                bNotFound = false;
                break;
            }
        }
        // If the newNeuron from innovsFromCon is not in the local neurons then you can use this innovation in the genome
        if (bNotFound){
            return make_tuple(con->innovsFromCon[i]->newNeuron,con->innovsFromCon[i]->newCon1,con->innovsFromCon[i]->newCon2);
        }
    }
    // No existing innovation found so add it
    shared_ptr<GNeuron> newNeuron = AddGlobalNeuron(depth);
    shared_ptr<GConnection> con1;
    shared_ptr<GConnection> con2;
    bool a,b; // Not used
    // Create new connection to new neuron
    tie(a,con1) = AddGlobalConnection(con->gIn,newNeuron);
    // Create new connection from new neuron
    tie(b,con2) = AddGlobalConnection(newNeuron,con->gOut);
    innovationsAddNeuron.emplace_back(make_shared<InnovationAddNeuron>(newNeuron,con1,con2));
    con->innovsFromCon.emplace_back(innovationsAddNeuron.back());
    return make_tuple(innovationsAddNeuron.back()->newNeuron,innovationsAddNeuron.back()->newCon1,innovationsAddNeuron.back()->newCon2);
}

