//
// Created by joe on 28/06/15.
//

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Population.h"

TEST_CASE( "Population tests", "[population]" ) {
    Population* pop = new Population(7,3,3);

    REQUIRE( pop->genomes.size() == 3 );
    REQUIRE( pop->globalNeurons.size() == 10 );
    REQUIRE( pop->globalLayers.size() == 2 );

    delete pop;
}