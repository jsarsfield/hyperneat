#include <iostream>
#include "Population.h"

using namespace std;

int main() {
    cout << "Begin evolution!" << endl;

    Population *pop = new Population(7, 3, 100);

    delete pop;
    return 0;
}
